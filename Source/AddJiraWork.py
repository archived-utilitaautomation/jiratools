try:
    
    from jira.client import JIRA
    
    jira_options={'server': 'https://utilita.atlassian.net'}
    
    jira=JIRA(options=jira_options,basic_auth=('jamesmcintyre@utilita.co.uk','pfx4ReekA2oWosIFDMnsB720'))
    
    issue = jira.issue('PA-332')
    
    import datetime
    
    t = input("What ticket would you like to log work for? (E.g.:PA-332):")
    d = input("What date did you do the work? (Format:DD/MM/YY):")
    w = input("How much time would you like to log to this ticket? (E.g.:1h):")
    
    
    d = datetime.datetime.strptime(d, "%d/%m/%y").date()
    
    jira.add_worklog(t,timeSpent=w,started=d)
    
    import ctypes
    ctypes.windll.user32.MessageBoxW(0, 'Work logged succesfully!', "Status", 0)
except Exception as e:
    import ctypes
    ctypes.windll.user32.MessageBoxW(0, str(e), "Error", 0)